import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

import App from './vue/App'
import login from './vue/login'
import profile from './vue/profile'
import chat from './vue/chat'
import notelist from './vue/notelist'

const routes = [
	{ path: '/', component: chat },
	{ path: '/login', component: login },
	{ path: '/profile', component: profile },
	{ path: '/notelist', component: notelist }
]

const router = new VueRouter({ routes })

export default router