import loading from 'vue-spinner/src/SquareLoader.vue' 
export default {
	data() {
		return {
			loading: false
		}
	},
	components: {loading}
}