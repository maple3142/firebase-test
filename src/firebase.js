import firebase from 'firebase'
/* config firebase here */
const config = {
	apiKey: 'AIzaSyCx10lXzKkqm7rJBEs5sJSlrATRBq5HsMk',
	authDomain: 'maple-3142.firebaseapp.com',
	databaseURL: 'https://maple-3142.firebaseio.com',
	projectId: 'maple-3142',
	storageBucket: 'maple-3142.appspot.com',
	messagingSenderId: '1068868113438'
}
firebase.initializeApp(config)
export default firebase

if (__DEV__)
	window.firebase = firebase