import 'bootstrapmin'
import 'jquery'
import 'popper.js'
import 'bootstrap'

import './style.less'

import Vue from 'vue'
import moment from 'moment'
import firebase from './firebase'
import Vuefire from 'vuefire'
Vue.use(Vuefire)

import router from './router.js'
import App from './vue/App'

moment.locale(navigator.language)
var db = firebase.database()

var app=new Vue({
	el: '#app',
	components: { App },
	router
})

if (__DEV__) { 
	window.app = app
	window.addAdmin = () => {
		db.ref('admin').push(app.$children[0].user.uid)
	}
}