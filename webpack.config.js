var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = {
	entry: ['./src/index.js'],
	output: {
		path: __dirname + '/dist',
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				loader: 'eslint-loader',
				enforce: 'pre',
				test: /(\.js$|\.vue$)/,
				exclude: /node_modules/
			},
			{
				loader: 'babel-loader',
				test: /\.js$/,
				exclude: /node_modules/
			},
			{
				loader: 'vue-loader',
				test: /\.vue$/
			},
			{
				loader: 'style-loader!css-loader',
				test: /\.css/
			},
			{
				loader: 'style-loader!css-loader!less-loader',
				test: /\.less/
			}
		]
	},
	resolve: {
		alias: {
			vue: process.env.NODE_ENV === 'development' ? 'vue/dist/vue.js' : 'vue/dist/vue.min.js',
			bootstrapmin: 'bootstrap/dist/css/bootstrap.min.css'
		},
		extensions: ['*', '.js', '.vue', '.css', '.less']
	},
	plugins: [
		new webpack.DefinePlugin({
			'__DEV__': process.env.NODE_ENV === 'development'
		}),
		new webpack.LoaderOptionsPlugin({
			options: {
				eslint: {
					configFile: './.eslintrc'
				}
			}
		}),
		new webpack.ProvidePlugin({
			jQuery: 'jquery',
			$: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default']
		}),
		new webpack.optimize.UglifyJsPlugin({
			output: {
				comments: false
			}
		}),
		new HtmlWebpackPlugin({
			title: 'Website',
			filename: 'index.html',
			template: 'src/index.ejs',
			minify: {
				collapseWhitespace: true
			}
		}),
		new CopyWebpackPlugin([
			{
				from: 'src/svg',
				to: 'svg'
			}
		])
	],
	devServer: {
		port: process.env.PORT || 3000,
		contentBase: './dist',
		inline: true,
		stats: 'errors-only'
	}
}